A `mongoengine <http://mongoengine.org/>`__ integration for `Graphene <http://graphene-python.org/>`__.

Examples
--------

Here is a simple Mongoengine document:

.. code:: python

    from mongoengine import Document, StringField, IntField

    
    class UserDoc(Document):

        name = StringField()
        last_name = StringField()
        age = IntField()

To create a GraphQL schema for it you simply have to write the
following:

.. code:: python

    from graphene_mongoengine import MongoEngineObjectType
    import graphene

    
    class User(MongoEngineObjectType):
        class Meta:
            document = UserDoc

    
    class Query(graphene.ObjectType):
        users = graphene.List(User)

    
    schema = graphene.Schema(query=Query)
