# -*- coding: utf-8 -*-
import inspect
from collections import OrderedDict

from graphene import Field
from graphene.types.objecttype import ObjectType, ObjectTypeOptions
from graphene.types.utils import yank_fields_from_attrs
from graphene.relay import Connection, Node

from graphene_mongoengine.converter import convert_mongoengine_field
from graphene_mongoengine.utils import is_mapped
from graphene_mongoengine.registry import Registry, get_global_registry


def construct_fields(
        document, registry, only_fields, exclude_fields):

    all_fields = document._fields.values()
    fields = OrderedDict()

    for field in all_fields:
        is_not_in_only = only_fields and field.name not in only_fields
        is_excluded = field.name in exclude_fields
        if is_not_in_only or is_excluded:
            # We skip this field if we specify only_fields and is not
            # in there. Or when we exclude this field in exclude_fields
            continue
        converted_field = convert_mongoengine_field(
            field, registry=registry)
        fields[field.name] = converted_field
    return fields


class MongoEngineObjectTypeOptions(ObjectTypeOptions):

    document = None
    registry = None
    connection = None
    id = None


class MongoEngineObjectType(ObjectType):

    @classmethod
    def __init_subclass_with_meta__(
            cls, document=None, registry=None, skip_registry=False,
            only_fields=(), exclude_fields=(), connection=None,
            use_connection=None, interfaces=(), id=None, **options):
        if not (inspect.isclass(document) and is_mapped(document)):
            raise Exception(
                'Provided document in %s is not a '
                'MongoEngine document' % cls.__name__)

        if not registry:
            registry = get_global_registry()
        assert isinstance(
            registry, Registry), (
                'The attribute registry in %s.Meta needs to be an '
                'instance of Registry, received "%s".' % (
                    cls.__name__, registry))

        mongoengine_fields = yank_fields_from_attrs(
                 construct_fields(
                     document, registry, only_fields, exclude_fields),
                 _as=Field)

        if use_connection is None and interfaces:
            use_connection = any((
                issubclass(interface, Node) for interface in interfaces))

        if use_connection and not connection:
            # We create the connection automatically
            connection = Connection.create_type(
                    '%sConnection' % cls.__name__, node=cls)

        if connection is not None:
            assert issubclass(connection, Connection), (
                "The connection must be a Connection. Received %s" % (
                    connection.__name__))

        _meta = MongoEngineObjectTypeOptions(cls)
        _meta.document = document
        _meta.registry = registry
        _meta.fields = mongoengine_fields
        _meta.connection = connection
        _meta.id = id or 'id'

        super(MongoEngineObjectType, cls).__init_subclass_with_meta__(
            _meta=_meta, interfaces=interfaces, **options)

        if not skip_registry:
            registry.register(cls)

    @classmethod
    def is_type_of(cls, root, info):
        if isinstance(root, cls):
            return True
        if not is_mapped(type(root)):
            raise Exception(
                'Received incompatible instance "%s".' % root)
        return isinstance(root, cls._meta.document)

    @classmethod
    def get_node(cls, info, id):
        return cls._meta.document.objects.get(id=id)

    def resolve_id(self, info):
        return self.pk
