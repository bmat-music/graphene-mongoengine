# -*- coding: utf-8 -*-
import unittest

from mongoengine import fields
from graphene import Boolean, Float, Int, String, List, Dynamic, Field, Enum
from graphene.types.datetime import DateTime

from graphene_mongoengine.registry import Registry
from graphene_mongoengine.converter import convert_mongoengine_field
from graphene_mongoengine import MongoEngineObjectType
from graphene_mongoengine.tests.documents import (
    Artist, Song, ArtistType, Collaboration)


registry = Registry()


class TestConverter(unittest.TestCase):

    def test_should_raise_exception(self):
        self.assertRaises(Exception, convert_mongoengine_field, None)

    def test_should_convert_to_string(self):
        self._check_convert(fields.StringField(), String())
        self._check_convert(fields.URLField(), String())
        self._check_convert(fields.EmailField(), String())
        self._check_convert(fields.BinaryField(), String())
        self._check_convert(fields.FileField(), String())
        self._check_convert(fields.UUIDField(), String())

    def test_should_convert_to_datetime(self):
        self._check_convert(fields.DateTimeField(), DateTime())
        self._check_convert(fields.ComplexDateTimeField(), DateTime())

    def test_should_convert_int(self):
        self._check_convert(fields.IntField(), Int())
        self._check_convert(fields.LongField(), Int())

    def test_should_convert_to_boolean(self):
        self._check_convert(fields.BooleanField(), Boolean())

    def test_should_convert_to_float(self):
        self._check_convert(fields.FloatField(), Float())
        self._check_convert(fields.DecimalField(), Float())

    def test_should_convert_to_list(self):
        self._check_convert(fields.ListField(fields.IntField), List(Int))

    def test_should_convert_to_enum(self):
        field = convert_mongoengine_field(Artist.type)
        self.assertTrue(isinstance(field, Enum))
        self.assertEqual(field.Band.value, ArtistType.Band)
        self.assertEqual(field.Singer.value, ArtistType.Singer)

    def test_should_convert_to_objecttype(self):

        class A(MongoEngineObjectType):

            class Meta:
                document = Artist
                registry = registry

        class B(MongoEngineObjectType):

            class Meta:
                document = Song
                registry = registry

        dynamic_field = convert_mongoengine_field(Song.artist, registry)
        self.assertIsInstance(dynamic_field, Dynamic)
        graphene_type = dynamic_field.get_type()
        self.assertIsInstance(graphene_type, Field)
        self.assertEqual(graphene_type.type, A)

    def test_should_convert_to_objecttype_list(self):

        class A(MongoEngineObjectType):

            class Meta:
                document = Artist
                registry = registry

        class B(MongoEngineObjectType):

            class Meta:
                document = Collaboration
                registry = registry

        dynamic_field = convert_mongoengine_field(
            Collaboration.artists, registry)
        self.assertIsInstance(dynamic_field, Dynamic)
        graphene_type = dynamic_field.get_type()
        self.assertIsInstance(graphene_type, List)
        self.assertEqual(graphene_type.of_type, A)

    def _check_convert(self, mf, gf):
        self.assertEqual(convert_mongoengine_field(mf), gf)
