# -*- coding: utf-8 -*-
import unittest
from graphene import Int, ObjectType, Schema
from graphene.relay import Node, is_node

from graphene_mongoengine.registry import Registry
from graphene_mongoengine import MongoEngineObjectType
from graphene_mongoengine.tests.documents import Artist, Song


registry = Registry()


class Performance(MongoEngineObjectType):

    class Meta:
        document = Artist
        registry = registry


class Track(MongoEngineObjectType):

    num = Int()

    class Meta:
        document = Song
        registry = registry
        exclude_fields = ('id')
        interfaces = (Node,)


class RootQuery(ObjectType):
    node = Node.Field()


schema = Schema(query=RootQuery, types=[Performance, Track])


class TestTypes(unittest.TestCase):

    def test_objecttype_register(self):
        self.assertTrue(issubclass(Track, ObjectType))
        self.assertEqual(Track._meta.document, Song)
        self.assertEqual(
            sorted(list(Track._meta.fields.keys())),
            sorted(['id', 'title', 'artist', 'num']))
        self.assertTrue(is_node(Track))
        self.assertEqual(
            sorted(list(Performance._meta.fields.keys())),
            sorted(['type', 'name']))
        self.assertFalse(is_node(Performance))

    def test_schema_representation(self):
        expected = '''schema {
  query: RootQuery
}

enum ArtistType {
  Band
  Singer
}

interface Node {
  id: ID!
}

type Performance {
  type: ArtistType
  name: String
}

type RootQuery {
  node(id: ID!): Node
}

type Track implements Node {
  title: String
  artist: Performance
  id: ID!
  num: Int
}
'''.lstrip()
        self.assertEqual(str(schema), expected)
