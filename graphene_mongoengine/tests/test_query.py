# -*- coding: utf-8 -*-
import unittest
import graphene

from graphene_mongoengine import MongoEngineObjectType
from graphene_mongoengine.tests.documents import Artist, Song, ArtistType


class TestQuery(unittest.TestCase):

    def test_simple_query_object(self):

        class SongObject(MongoEngineObjectType):

            class Meta:
                document = Song
                only_fields = ('id',)

        class Query(graphene.ObjectType):
            song = graphene.Field(SongObject)

            def resolve_song(self, info):
                return Song(id='uuid')

        schema = graphene.Schema(query=Query)
        query = '''
            query {
              song {
                id
              }
            }
        '''
        result = schema.execute(query)
        self.assertFalse(result.errors)
        self.assertEqual(result.data, {'song': {'id': 'uuid'}})

    def test_ref_query_object(self):

        class ArtistObject(MongoEngineObjectType):

            class Meta:
                document = Artist

        class SongObject(MongoEngineObjectType):

            class Meta:
                document = Song

        class Query(graphene.ObjectType):
            song = graphene.Field(SongObject)

            def resolve_song(self, info):
                art = Artist(name='artist', type=ArtistType.Band)
                return Song(id='uuid', artist=art, title='title')

        schema = graphene.Schema(query=Query)
        query = '''
            query {
              song {
                id
                title
                artist {
                  name
                  type
                }
              }
            }
        '''
        result = schema.execute(query)
        self.assertFalse(result.errors)
        self.assertEqual(result.data, {
            'song': {
                'id': 'uuid',
                'title': u'title',
                'artist': {
                    'name': u'artist',
                    'type': u'Band'
                    }
                }
            })
