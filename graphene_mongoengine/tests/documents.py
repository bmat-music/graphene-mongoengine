# -*- coding: utf-8 -*-
from mongoengine import (
    Document, StringField, EmbeddedDocument, EmbeddedDocumentField,
    EmbeddedDocumentListField,)
from enum import Enum

from graphene_mongoengine import EnumField


class ArtistType(Enum):

    Band = 1
    Singer = 2


class Artist(EmbeddedDocument):

    name = StringField()
    type = EnumField(ArtistType)


class Song(Document):

    title = StringField()
    artist = EmbeddedDocumentField(Artist)


class Collaboration(Document):

    name = StringField()
    artists = EmbeddedDocumentListField(Artist)
