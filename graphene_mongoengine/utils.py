# -*- coding: utf-8 -*-
from mongoengine.document import EmbeddedDocument, Document


def is_mapped(obj):
    return issubclass(obj, EmbeddedDocument) or issubclass(obj, Document)
