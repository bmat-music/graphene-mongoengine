# -*- coding: utf-8 -*-
from singledispatch import singledispatch
from mongoengine import fields
from graphene import (
    Boolean, Float, Int, String, List, Dynamic, Field, Enum)
from graphene.types.datetime import DateTime
from graphene.relay import is_node

from graphene_mongoengine.fields import EnumField, MongoEngineConnectionField


@singledispatch
def convert_mongoengine_field(field, registry=None):
    raise Exception(
        "Don't know how to convert the "
        "MongoEngine field %s (%s)" % (field, field.__class__))


@convert_mongoengine_field.register(fields.StringField)
@convert_mongoengine_field.register(fields.URLField)
@convert_mongoengine_field.register(fields.EmailField)
@convert_mongoengine_field.register(fields.BinaryField)
@convert_mongoengine_field.register(fields.FileField)
@convert_mongoengine_field.register(fields.UUIDField)
def convert_field_to_string(field, registry=None):
    return String()


@convert_mongoengine_field.register(fields.ComplexDateTimeField)
@convert_mongoengine_field.register(fields.DateTimeField)
def convert_field_to_datetime(field, registry=None):
    return DateTime()


@convert_mongoengine_field.register(fields.ObjectIdField)
def convert_field_to_id(field, registry=None):
    return String()


@convert_mongoengine_field.register(EnumField)
def convert_field_to_enum(field, registry=None):
    return Enum(field.enum.__name__, field.enum.__members__.items())()


@convert_mongoengine_field.register(fields.IntField)
@convert_mongoengine_field.register(fields.LongField)
def convert_field_to_int(field, registry=None):
    return Int()


@convert_mongoengine_field.register(fields.BooleanField)
def convert_field_to_boolean(field, registry=None):
    return Boolean()


@convert_mongoengine_field.register(fields.FloatField)
@convert_mongoengine_field.register(fields.DecimalField)
def convert_field_to_float(field, registry=None):
    return Float()


@convert_mongoengine_field.register(fields.ListField)
@convert_mongoengine_field.register(fields.SortedListField)
def convert_field_to_graphql_list(field, registry=None):
    graphene_type = type(convert_mongoengine_field(field.field()))
    return List(graphene_type)


@convert_mongoengine_field.register(fields.EmbeddedDocumentListField)
def convert_field_to_graphql_list_field(field, registry=None):
    document = field.field.document_type

    def dynamic_type():
        typ = registry.get_type_for_model(document)
        if not typ:
            return None
        if is_node(typ):
            return MongoEngineConnectionField(typ)
        return List(typ)

    return Dynamic(dynamic_type)


@convert_mongoengine_field.register(fields.EmbeddedDocumentField)
@convert_mongoengine_field.register(fields.ReferenceField)
def convert_field_to_graphql_field(field, registry=None):
    document = field.document_type

    def dynamic_type():
        typ = registry.get_type_for_model(document)
        if not typ:
            return None
        return Field(typ)

    return Dynamic(dynamic_type)
