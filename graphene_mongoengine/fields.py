# -*- coding: utf-8 -*-
from functools import partial
from mongoengine import QuerySet as MongoQuerySet
from mongoengine.fields import IntField
from graphene.relay import ConnectionField
from graphene.relay.connection import PageInfo
from graphql_relay.connection.arrayconnection import connection_from_list_slice


class EnumField(IntField):
    """
    A class to register Enum type (from the package enum34) into mongo
    :param enum: must be of :class:`enum.Enum`: type
    and will be used as possible choices
    """

    def __init__(self, enum, *args, **kwargs):
        self.enum = enum
        super(EnumField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        if isinstance(value, int):
            return self.enum(super(EnumField, self).to_python(value))
        return value

    def to_mongo(self, value):
        return self._get_value(value)

    def prepare_query_value(self, op, value):
        return super(EnumField, self).prepare_query_value(
                op, self._get_value(value))

    def validate(self, value):
        ret = super(EnumField, self).validate(self._get_value(value))
        return ret

    def _validate(self, value, **kwargs):
        return super(EnumField, self)._validate(
                self.enum(self._get_value(value)), **kwargs)

    def _get_value(self, enum):
        return enum.value if hasattr(enum, 'value') else enum


class MongoEngineConnectionField(ConnectionField):

    @property
    def document(self):
        return self.type._meta.node._meta.document

    @classmethod
    def connection_resolver(
            cls, resolver, connection, document, root, info, **args):
        iterable = resolver(root, info, **args)
        if iterable is None:
            iterable = document.objects
        if isinstance(iterable, MongoQuerySet):
            count = iterable.count()
        else:
            count = len(iterable)
        return connection_from_list_slice(
            iterable,
            args,
            slice_start=0,
            list_length=count,
            list_slice_length=count,
            connection_type=connection,
            pageinfo_type=PageInfo,
            edge_type=connection.Edge)

    def get_resolver(self, parent_resolver):
        return partial(
            self.connection_resolver, parent_resolver,
            self.type, self.document)
