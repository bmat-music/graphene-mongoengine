from setuptools import find_packages, setup

setup(
    name='graphene-mongoengine',
    version='0.0.4',

    description='Graphene MongoEngine integration',
    long_description=open('README.rst').read(),

    url='https://bitbucket.org/bmat-music/graphene-mongoengine',

    author='BMAT Developers',
    author_email='vericast-sw@bmat.com',

    license='GPLv3',

    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Libraries',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.4'
    ],

    keywords='api graphql protocol rest relay graphene mongo mongoengine',

    packages=find_packages(exclude=['tests']),

    install_requires=[
        'six>=1.10.0',
        'graphene>=2.0',
        'mongoengine',
        'singledispatch>=3.4.0.3',
        'iso8601',
        'enum34'
    ],
    tests_require=[
        'nose'
    ],
)
